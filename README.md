# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database. Project 6 adds on 
a RESTful service to expose the data stored in MongoDB, and a consumer program will
allow the user to view the stored data.

Author: Jimmy Lam, jimmyl@uoregon.edu

## Viewing the exposed data

The server side of things will run on port 5001, and the consumer program will run on port 5000.

First, go to "http://0.0.0.0:5001" to store data into the database.

To see the exposed data, use one of the following links (using port 5001):

* Without specifying the return format:
    * "http://0.0.0.0:5001/listAll" should return all open and close times in the database in JSON format
    * "http://0.0.0.0:5001/listOpenOnly" should return open times only in JSON format
    * "http://0.0.0.0:5001/listCloseOnly" should return close times only in JSON format
    
* With specifying the return format:
    * "http://0.0.0.0:5001/listAll/csv" should return all open and close times in CSV format
    * "http://0.0.0.0:5001/listOpenOnly/csv" should return open times only in CSV format
    * "http://0.0.0.0:5001/listCloseOnly/csv" should return close times only in CSV format
    * "http://0.0.0.0:5001/listAll/json" should return all open and close times in JSON format
    * "http://0.0.0.0:5001/listOpenOnly/json" should return open times only in JSON format
    * "http://0.0.0.0:5001/listCloseOnly/json" should return close times only in JSON format
    
* To query parameters to get the top "k" open and close times:
    * "http://0.0.0.0:5001/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://0.0.0.0:5001/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://0.0.0.0:5001/listCloseOnly/csv?top=6" should return top 6 close times only (in ascending order) in CSV format
    * "http://0.0.0.0:5001/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

**Note:** when retrieving a csv format of listAll, the open times are listed first, followed by the close times. 
If you want to differentiate between the two, simply split the csv data in half.

## Using the consumer program

To use the consumer program, go to "http://0.0.0.0:5000". 

First, it will ask the user to specify which 
times to retrieve (open only, close only, or open and close). Then, the user will be able to provide an
optional parameter to get the top "k" times. If the value entered is greater than the number of times
found in the database, then only the times found will be shown. Finally, there is an option to display
the raw unformatted data retrieved from the database. If JSON is selected, the JSON data will be
displayed below the formatted (in bullet points) data, and the same thing applies to the csv format.
This last option is mainly a debugging tool to make sure the data retrieved was really JSON or really csv
and not the other. 

Once all the parameters have been filled, click on the submit button to get the times. You can
also press the clear output button to clear the page.

If no times were found, the message "Couldn't find any times from the database" will be shown.