<html>
    <head>
        <title>Controle Times</title>
    </head>

    <h1>Welcome!</h1>
    <p>This page allows you to access the stored controle times from the database.</p>
    <ul>
        <?php
        if (array_key_exists('time_type', $_POST)) {
            if ($_POST["time_type"] == "open") {
                getOpen();
            }
            elseif ($_POST["time_type"] == "close") {
                getClose();
            }
            elseif ($_POST["time_type"] == "all") {
                getOpenAndClose();
            }
        }

        function getOpen() {
            if ($_POST["file_type"] == NULL) {
                $url = 'http://web/listOpenOnly/json';
            }
            else {
                $url = 'http://web/listOpenOnly/' . $_POST["file_type"];
            }

            if ($_POST["top"] != NULL) {
                $url = $url . '?top=' . $_POST["top"];
            }

            if ($_POST["file_type"] == "csv") {
                $csv = file_get_contents($url);
                $times = str_getcsv($csv);
            }
            else {
                $json = file_get_contents($url);
                $obj = json_decode($json);
                $times = $obj->open;
            }
            printResults($times, "open", $url, 1);
        }

        function getClose() {
            if ($_POST["file_type"] == NULL) {
                $url = 'http://web/listCloseOnly/json';
            }
            else {
                $url = 'http://web/listCloseOnly/' . $_POST["file_type"];
            }

            if ($_POST["top"] != NULL) {
                $url = $url . '?top=' . $_POST["top"];
            }

            if ($_POST["file_type"] == "csv") {
                $csv = file_get_contents($url);
                $times = str_getcsv($csv);
            }
            else {
                $json = file_get_contents($url);
                $obj = json_decode($json);
                $times = $obj->close;
            }
            printResults($times, "close", $url, 1);
        }

        function getOpenAndClose() {
            if ($_POST["file_type"] == NULL) {
                $url = 'http://web/listAll/json';
            }
            else {
                $url = 'http://web/listAll/' . $_POST["file_type"];
            }

            if ($_POST["top"] != NULL) {
                $url = $url . '?top=' . $_POST["top"];
            }
            if ($_POST["file_type"] == "csv") {
                $csv = file_get_contents($url);
                $times = str_getcsv($csv);
                $len = count($times);
                $open_times = array_slice($times, 0, $len / 2);
                $close_times = array_slice($times, $len / 2);
            }
            else {
                $json = file_get_contents($url);
                $obj = json_decode($json);
                $open_times = $obj->open;
                $close_times = $obj->close;
            }
            printResults($open_times, "open", $url, 1, 0);
            printResults($close_times, "close", $url, 0);
        }

        function printResults($times, $open_or_close, $url, $print_error=0, $print_raw=1) {
            if (count($times) == 0 and $print_error) {
                echo "<h3>Couldn't find any times from the database.<h3>";

            }
            else {
                echo "<h3>Got the following " . $open_or_close . " times</h3>";
                // print each time
                foreach ($times as $t) {
                    echo "<li>$t</li>";
                }
            }
            // print the raw data if user specified to
            if ($_POST["file_type"] != NULL && $print_raw) {
                printRaw($url);
            }
        }

        function printRaw($url) {
            $data = file_get_contents($url);
            echo "<h3>Got the following raw data from the database</h3>";
            echo $data;
        }
        ?>
    </ul>

    <form method=POST>
        <br>
        <p>1. Select which time to retrieve. </p>
        Select time:
        <select name="time_type">
            <option value="open">Open Only</option>
            <option value="close">Close Only</option>
            <option value="all">Open and Close</option>
        </select>
        <br>

        <p>2. (optional) Enter a numeric value if you wish to get the top "k" times.
        <br>&nbsp&nbsp&nbsp Note: if the database contains fewer than the "k" value provided,
        <br>&nbsp&nbsp&nbsp only the times found will be shown.
        </p>
        Top "k" times:
        <input type="number" min=0 max=20 name="top" ></input>

        <br>

        <p>3. (optional) Select a file format (JSON or CSV) to display the unformatted data.
        <br>&nbsp&nbsp&nbsp This will display the data gathered directly from the server below the
        <br>&nbsp&nbsp&nbsp formatted open or close times. This is mainly a debug tool to make
        <br>&nbsp&nbsp&nbsp sure the data is in the correct format.
        </p>
        File format:
        <select name="file_type">
            <option></option>
            <option value="json">JSON</option>
            <option value="csv">CSV</option>
        </select>
        <br><br>

        <input type="submit"></input>
    </form>

    <form method=POST>
        <input type="submit" name="RESET" value="Clear Output">
    </form>

    
</body>
</html>
